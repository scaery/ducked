# Ducked

![Bash Bunny](https://web.archive.org/web/20210407205426if_/https://camo.githubusercontent.com/24a8c593ac5cb7c65483291f1a9728d3c917514159ef61877527392df58364ff/68747470733a2f2f7777772e68616b352e6f72672f77702d636f6e74656e742f75706c6f6164732f323031372f31302f69636f6e332d313639783136392e706e67)

Ducked is a customized Rubberducky Payload for BashBunny Firmware 1.6_305

* Redteam alike!
* Stay under the radar!
* Use for educational purposes only!

## Installation

### Clone the repo

```
git clone https://github.com/scaery/ducked.git
```

### Move files to BashBunny

Caution! 
** Make sure you have a backup of your current payloads!
** Your pendrive needs to be named "BashBunny".

```
rm -rf ducked/.git
cp -R ducked/* $(lsblk -o mountpoint | grep 'BashBunny')
mv -t $(lsblk -o mountpoint | grep 'BashBunny')/payloads/ ducked/README.md ducked/LICENSE.md
rm -rf ducked
sync; umount $(lsblk -o mountpoint | grep 'BashBunny')
```

### Switch 1)

Stores loot from the Windows target!

* Winenum Script modified
* Procdump LSASS minidump (load mimikatz.exe and use sekurlsa:minidump lsass.dmp)
* Wifi Credential Ripper

Fire up ![Mimikatz](https://github.com/gentilkiwi/mimikatz/releases)
```
sekurlsa:minidump lsass.dmp
```

### Switch 2)

ICMPSH - Drop a shell from Windows target over ICMP!

* Project: https://github.com/scaery/icmpsh

Make sure you change the IP address <Server IP missing> in duck_code.txt Line 35 
to whatever IP address the attacker machine or public VPS is listening on!

### BashBunny Switches

![Switches](https://web.archive.org/web/20180814005714/https://wiki.bashbunny.com/images/bb_diagram1.png)

## Contribute

Submit issues, add feature requests or pull requests.

## ToDo

* Remove security events once compromised and hide all traces
* Execute everything in memory with cscript
* Check WS-ResetBypass again
* Obfuscate payloads with cradle crafter

## License

This project is licensed under WTFWPL - see the ![License](LICENSE.md) file for more details.

Thanks to Darren Kitchen from Hak5 providing this awesome device.
